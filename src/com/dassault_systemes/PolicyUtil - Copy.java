package com.dassault_systemes.enovia.cgr;
//package com.dassault_systemes.enovia.cgr;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.StringTokenizer;
//import java.util.StringTokenizer;

//import java.util.StringTokenizer;
//import java.util.StringTokenizer;
//import java.util.StringTokenizer;

import com.google.gson.Gson;

public class PolicyUtil {

	
	static ArrayList arrayList = new ArrayList();
	
	public static void main(String[] args) throws IOException {
		
		
		
		FileInputStream fstream = new FileInputStream("e:\\policy.txt");
		DataInputStream in = new DataInputStream(fstream);
		  BufferedReader br = new BufferedReader(new InputStreamReader(in));
		  String strLine;
		
		  //Read File Line By Line
		  String state = null;
		 while ((strLine = br.readLine()) != null)   {
			  if(strLine != null && strLine.contains("state ")){
				  StringTokenizer tokens = new StringTokenizer(strLine);
				    String[] splited = new String[tokens.countTokens()];
				    int index = 0;
				    while(tokens.hasMoreTokens()){
				        splited[index] = tokens.nextToken();
				        ++index;
				    }
				  state = splited[1];
			  }
		  if(strLine != null && strLine.contains("user VPLM")){
			  String strReturn = processLines(strLine,state);
			System.out.println("Access-->" + strReturn);
		  }
		  }
		  
		//  String strReturn = processLines("login user VPLMTeamManager key ExtendedSingleCtxChangesovAccess_AnyOrgSinglePrj changesov single project",state);
		  
		System.out.println(arrayList);
		Gson gson = new Gson();
		String json = gson.toJson(arrayList); 
		System.out.println(json);
	}

	
	public static String processLines (String strInput, String state){
		//String strInput = "login user VPLMProjectAdministrator key ExtendedSingleCtxModifyAccess_AnyOrgSinglePrj modify,checkin,changename single project inclusive reserve";
		//System.out.println(strInput);
		Map<String, String> map = new HashMap<>();
		ArrayList<String> words = new ArrayList<>();
		map.put("state", state);
		StringTokenizer stringTokenizer = new StringTokenizer(strInput);
		
		while (stringTokenizer.hasMoreElements()){
			
			words.add(stringTokenizer.nextToken());
		}
		if(strInput.contains("login")){
			map.put("login", "Y");
			strInput = strInput.replace("login ", "");
		}
		if(strInput.contains(" key")){
			int index = words.indexOf("key");
			String value = words.get(index+1);
			map.put("key", value);
			strInput = strInput.replace(" key", "");
			strInput = strInput.replace(" "+ value, "");
		}
		if(strInput.contains("user")){
			int index = words.indexOf("user");
			String value = words.get(index+1);
			map.put("user", value);
			strInput = strInput.replace("user ", "");
			strInput = strInput.replace(value + " ", "");
		}
		if(strInput.contains(" project")){
			int index = words.indexOf("project");
			String value = words.get(index-1);
			map.put("project", value);
			strInput = strInput.replace(" project", "");
			strInput = strInput.replace(" "+ value, "");
		}
		
		if(strInput.contains(" organization")){
			int index = words.indexOf("organization");
			String value = words.get(index-1);
			map.put("organization", value);
			strInput = strInput.replace(" organization", "");
			strInput = strInput.replace(" "+ value, "");
		}
		
		if(strInput.contains(" maturity")){
			int index = words.indexOf("maturity");
			String value = words.get(index-1);
			map.put("maturity", value);
			strInput = strInput.replace(" maturity", "");
			strInput = strInput.replace(" "+ value, "");
		}
		
		if(strInput.contains(" filter")){
			int index = strInput.indexOf("filter");
			String value = strInput.substring(index + "filter ".length(), strInput.length());
			map.put("filter", value);
			strInput = strInput.substring(0, index);
		}
		
		if(strInput.contains(" owner")){
			int index = words.indexOf("owner");
			String value = words.get(index-1);
			map.put("owner", value);
			strInput = strInput.replace(" owner", "");
			strInput = strInput.replace(" "+ value, "");
		}
	
		if(strInput.contains(" unreserve")){
			map.put("reserve", "no");
			strInput = strInput.replace(" unreserve", "");
		}
		
		if(strInput.contains(" reserve")){
			int index = words.indexOf("reserve");
			String value = words.get(index-1);
			if("no".equals(value)){
				map.put("reserve", "no");	
				strInput = strInput.replace(" no", "");
			}
			else if("context".equals(value)){
				map.put("reserve", "context");	
				strInput = strInput.replace(" context", "");
			}
			else if("inclusive".equals(value)){
				map.put("reserve", "inclusive");	
				strInput = strInput.replace(" inclusive", "");
			}
			else
			{
				map.put("reserve", "yes");
			}
			strInput = strInput.replace(" reserve", "");
		}

		
		strInput = strInput.trim();
		StringTokenizer stringTokenizer2 = new StringTokenizer(strInput, ",");
		while (stringTokenizer2.hasMoreElements()){
			String strToken = stringTokenizer2.nextToken();
			map.put(strToken, "Y");
			//strInput = strInput.replace(strToken, "");
		}
	//	strInput = strInput.replace(",", "");
		//	New commented line strInput = strInput.replace(",", "");
		arrayList.add(map);
		return strInput;
		
		
	}
}
